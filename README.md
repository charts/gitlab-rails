# GitLab Chart (deprecated)
This chart is no longer supported, and has been replaced by the cloud-native [`gitlab` chart](https://gitlab.com/charts/gitlab).
